import java.util.Scanner;

public class Schleife_1 {


	       static Scanner tastatur = new Scanner(System.in);

	        public static int eingabe() {
	            return tastatur.nextInt();}

	    public static void main(String[] args) {

	    	//______________________________
	        // Aufgabe 1
	    	//______________________________
	        System.out.println("Aufgabe 1");

	        int zahl = 0;
	        System.out.println("Geben sie eine zweite Zahl ein:");
	        zahl = eingabe();

	        //Aufgabe a)
	        for(int i=1;i<=zahl;i++)    {
	            System.out.print(i + " ");
	        }

	        System.out.print("");

	        //Aufgabe b)
	        for(int i=0;zahl>=1;zahl--)    {
	            System.out.print(zahl + " ");
	        }
	        System.out.println("");
	        
	        //______________________________
	        //Aufgabe 2
	        //______________________________
	        
	        System.out.println("Aufgabe 2");

	        zahl = 0;
	        System.out.println("Geben sie eine Zahl ein: ");
	        zahl = eingabe();
	        int zaehler = 0;
	        int berechnungsZahl = 1;

	        //Aufgabe a
	        for(int i = 0;i<zahl;i++) {
	            zaehler = zaehler + berechnungsZahl;
	            berechnungsZahl = berechnungsZahl ++;
	        }
	        System.out.println(zaehler);

	        berechnungsZahl = 0;
	        zaehler = 0;

	        //Aufgabe b
	        for(int i = 0;i<zahl;i++) {
	            zaehler = zaehler + berechnungsZahl;
	            berechnungsZahl = berechnungsZahl +  2;
	        }

	        zaehler = zaehler + (2*zahl);

	        System.out.println(zaehler);

	        berechnungsZahl = 1;
	        zaehler = 0;
	    }
}
