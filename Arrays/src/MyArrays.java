
public class MyArrays {

	public static void main(String[] args) {
		
		/*
		 * Auch deklarierbar und erzeugbar zur selben Zeit Beispiel: 
		 * int [ ] BeispielArray = new int[6];
		 */ 
		
		//*deklaration des Namen in dem Fall "meinArray" 
		int [ ] meinArray;
		
		//*Erzeugen/Initialisieren des Arrays dabei ist "10" nicht mehr veränderbar.
		meinArray = new int[10];
		
		meinArray [1] = 100;
		meinArray [2] = 100;
		meinArray [3] = 1000; 
		meinArray [4] = 100;
		meinArray [5] = 100;
		meinArray [6] = 100;
		meinArray [7] = 100;
		meinArray [8] = 100;
		meinArray [9] = 100;
		System.out.println(meinArray[4]);
	
	}

}
