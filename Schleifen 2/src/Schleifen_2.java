import java.util.Scanner;

public class Schleifen_2 {

		static Scanner tastatur = new Scanner(System.in);

		public static int eingabe() {
			return tastatur.nextInt();}

			public static void main(String[] args) {
				
				
				
			//__________________________________________
			//aufgabe 1
			//__________________________________________
				
			System.out.println("Aufgabe 1");
			int zahl = 0;
			System.out.println("Geben Sie eine zweite Zahl ein:");
			zahl = eingabe();
			
			//a)
			int i = 1;
			while(i <= zahl) {
				System.out.print(i + " ");;
				i++;
			}
			System.out.println("");
			//b)
			while(zahl >= 1) {
				System.out.print(zahl + " ");
				zahl--;
			}
			System.out.println("");
			
			
			
			
			//__________________________________________
			//aufgabe 2
			//__________________________________________
			
			System.out.println("Aufgabe 2");
					
			boolean eingabeKorrekt = false;
			int zaehler = 1;
			long ausgabe = 1;
			
			while (eingabeKorrekt == false) {
			System.out.println("Geben sie eine Zahl ein von: (0-20)");
			zahl = eingabe();
			if(zahl <=20) {eingabeKorrekt = true;}
			else{System.out.println("Die Eingabe ist falsch, bitte erneut probieren");}
			}
			
			do {
				ausgabe = ausgabe * zaehler;
				zaehler++;
			}while(zaehler <= zahl);
			
			System.out.println(ausgabe);
			
			
			
	}

}