import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag= 0;
       byte anzahlTickets;
       
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();

       System.out.println("anzahlTickets:");
       anzahlTickets = tastatur.nextByte();
       if(anzahlTickets >= 11 || anzahlTickets > 0)  {
       }
       else anzahlTickets = 1; 
    
       
       
       
       // Geldeinwurf
       // -----------
       zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f�\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       ausgabeFahrschein();
       
       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabegeld(r�ckgabebetrag, eingezahlterGesamtbetrag, zuZahlenderBetrag);
                 
    }    
    
    
    public static void ausgabeFahrschein () {      
    	// -----------------
    	System.out.println("\nFahrschein wird ausgegeben");
    	for (int i = 0; i < 8; i++) 							{
    		System.out.print("=");
    		try {
			Thread.sleep(250);
    	} 	catch (InterruptedException e) {
			e.printStackTrace();
		}
    		/*Eine Methode erstellt mit dem Namen "F*/
    }
    System.out.println("\n\n");
    }

    
    public static void r�ckgabegeld (double r�ckgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    
    	 r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
         if(r�ckgabebetrag > 0.0)
         {
      	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f � ", r�ckgabebetrag);
      	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

             while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
             {
          	  System.out.println("2 EURO");
  	          r�ckgabebetrag -= 2.0;
             }
             while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
             {
          	  System.out.println("1 EURO");
  	          r�ckgabebetrag -= 1.0;
             }
             while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
             {
          	  System.out.println("50 CENT");
  	          r�ckgabebetrag -= 0.5;
             }
             while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
             {
          	  System.out.println("20 CENT");
   	          r�ckgabebetrag -= 0.2;
             }
             while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
             {
          	  System.out.println("10 CENT");
  	          r�ckgabebetrag -= 0.1;
             }
             while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
             {
          	  System.out.println("5 CENT");
   	          r�ckgabebetrag -= 0.05;
             }
            
             System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                     "vor Fahrtantritt entwerten zu lassen!\n"+
                     "Wir w�nschen Ihnen eine gute Fahrt.");

    }
    
    }
}